#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iostream>

using namespace std;

//inverse연산을 위한 확장된 유클리드 알고리즘
//기존 과제의 내용 활용
__int64 inverse(__int64 num, __int64 R)
{
	//cout << "input number : " << std::hex << num << endl;

	//계산을 위한 변수들을 선언한다
	__int64 r = R;
	__int64 t = 0;
	__int64 newt = 1;
	__int64 newr = num;

	__int64 q = 0;

	__int64 tmpR = 0;
	__int64 tmpT = 0;

	//메인 알고리즘 영역
	while (newr != 0)
	{
		//다음 계산을 위한 r,t값을 보존한다
		tmpR = newr;
		tmpT = newt;

		//각 값을 구하기위한 연산 수행
		q = r / newr;
		newr = r - (q * newr);
		newt = t - (q * newt);

		//r,t값 update
		r = tmpR;
		t = tmpT;

	}
	
	if (t < 0)
		t = t + R;

	//cout << "inverse result : " << t << endl;
	return t;
}

//exponentiation 구현
__int64 exp(__int64 B, __int64 e, __int64 N)
{
	//사용할 변수
	__int64 b = B;
	__int64 res = 1;
	
	//밑을 계속 제곱하고 bit가 1일때마다 res에 곱한다
	while (e > 0)
	{
		//bit연산을 활용하여 비트값이 1일때마다 res에 곱한다
		if (1 & e)
		{
			res = (res*b) % N;
		}

		//밑을 계속 곱한다
		b = (b * b)%N;
		e = e >> 1;
	}

	//계산된 결과값 res return
	return res;
}

//최대공약수를 구하는 함수
__int64 gcd(__int64 m, __int64 n)
{
	__int64  tmp;

	//유클리드 알고리즘 사용
	while (n > 0)
	{
		tmp = m % n;
		m = n;
		n = tmp;
	}

	return m;
}

//밀러 라빈 소수판별법 구현
//이 방법은 확실한 판단이 아닌 높은 확률로 소수임을 판별하는것이다
//n이 작으므로 a=2,3에 대해서만 검사해보면 되지만 과제조건에 맞춰 20회의 테스트 하도록 구현했습니다
bool millerRabin(__int64 n)
{
	//2에 대한 처리가 제대로 되지 않아 따로 입력
	if (n == 2)
		return true;

	//계산에 사용될 변수들
	__int64 a, b, m, k = 0;
	
	//n-1 = (2^k)*m 을 만족하는 m, k를 구한다
	m = n - 1;
	while (m % 2 == 0)
	{
		m = m / 2;
		k++;
	}

	//n이 작으므로 a=2,3에 대해서만 검사해보면 되지만 과제조건에 맞춰 20회의 테스트 하도록 구현했습니다
	//과제 조건에 맞게 테스트를 20회 수행한다
	for (int i = 0; i<20; i++)
	{
		//먼저 랜덤한 수를 선택한다
		a = rand() % n;
		if (a<2)
			a = 2;

		//a와 n의 공약수가 존재하면 소수가 아니다
		if (gcd(a, n) != 1)
		{
			return false;
		}

		//b = a^m mod n
		b = exp(a, m, n);

		//강한 소수인지 판단
		if (b == 1 || b == (n - 1))
		{
			continue;       //조건이 맞으면 강한 유사소수이다.
		}
		else
		{
			for (int j = 0; j<k - 1; j++)
			{
				b = (b*b) % n;
				if (b == (n - 1))       //강한 유사소수이다.
					break;
			}
			//여기서 걸리면 소수가 아닌 경우이다
			if (b != (n - 1))
			{
				return false; //소수가 아니므로 false return
			}
		}
	}
	//강한 소수이다
	return true;

}

//소수인지 확인하는 함수
//밀러-라빈 함수 구현으로 사용하지 않았습니다
bool isPrime(__int64 num)
{
	__int64 root;

	root = (__int64)sqrt(num);

	for(__int64 i = 2; i <= root; i++)
	{
		if (num % i == 0)
			return false;
	}
	return true;
}

//소수를 만드는 함수 위에서 구현한 밀러-라빈함수를 사용
__int64 makePrime(__int64 size)
{
	__int64 num;
	//랜덤한 수를 생성하고 이 수가 소수일때까지 반복해서 생성, 소수인지 판별은 밀러-라빈함수 사용
	while (!millerRabin(num = rand() % size));
	//while (!isPrime(num = rand() % size));

	return num;
}

//E를 선택하는 함수
__int64 makeE(__int64 P)
{
	__int64 e;
	
	//수를 늘려 가장 작은 서로소인 숫자 E를 선택한다
	for (e = 2; e < P; e++)
	{
		if (gcd(P, e) == 1)
		{
			return e;
		}
	}
	return -1;
}

//RSA함수
bool RSA()
{
	//랜덤 시드값 초기화
	srand((unsigned int)time(NULL));
	
	//사용될 변수들 선언
	__int64  p, q, N, phi, e, d, c, m, m2;
	
	//p와 q를 생성한다
	p = makePrime((__int64)pow(2,15) - 1);
	q = makePrime((__int64)pow(2,15) - 1);
	
	//혹시나 p와 q의 값이 같을경우 처리
	if (p == q)
		while (p != (q = makePrime((__int64)pow(2, 15) - 1)));
	
	//p = 17293; q = 19051;
	cout << "p = " << p << "\nq = " << q << endl;
	
	//N을 구한다
	N = p*q;
	cout << "N = " << N << endl;

	//Phi를 구한다
	phi = (p-1) * (q-1);
	cout << "phi = " << phi << endl;

	//e와 d를 만든다
	e = makeE(phi);
	d = inverse(e, phi);

	cout << "e = " << e << endl;
	cout << "d = " << d << endl;
	
	//m을 입력받고 출력
	cout << "Message input : ";
	cin >> m;
	//m = 14852369;
	cout << "Message = " << m << endl;

	//암호화부분
	cout << "**Encryption" << endl;
	c = exp(m, e, N);
	cout << "cipher = " << c << endl;

	//복호화부분
	cout << "**Decryption" << endl;
	m2 = exp(c, d, N);
	cout << "decrypted cipher = " << m2 << endl;

	if (m == m2)
		return true;
	else
		return false;
}